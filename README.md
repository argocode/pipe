# Bitbucket Pipelines Pipe: Helm pipe

This pipe is used to deploy new image into kubernetes cluster with Helm.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: veracityins/pipe:0.1.1
    variables:
      APP:           "<string>"
      APP_DIR:       "<string>"
      NS:            "<string>"
      BRANCH:        "<string>"
      REPO_URL:      "<string>"
      DEST_DIR:      "<string>"
      DEPLOY_DIR:    "<string>"
      KUBECONFIG:    "<string>"
      TAG:           "<string>"
      REPO_USER_KEY: "<string>"
      # DRY_RUN:     "<boolean>" # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| APP (*)               | Helm application |
| APP_DIR (*)           | Helm application directory |
| NS (*)                | Kubernetes namespace |
| BRANCH (*)            | Deployments branch |
| REPO_URL (*)          | Full deployments repo URL for cloning |
| DEST_DIR (*)          | Directory where repo will be cloned |
| DEPLOY_DIR (*)        | Directory where deployments will be |
| KUBECONFIG (*)        | Kubeconfig file path |
| TAG (*)               | Release tag |
| REPO_USER_KEY (*)     | Base64 encoded private key to access repo |
| DRY_RUN               | Run deployment with --dry-run option. Default: `false`. |

_(*) = required variable._
