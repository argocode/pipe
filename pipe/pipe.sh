#!/usr/bin/env bash
#
source "$(dirname "$0")/common.sh" 

info "Executing the pipe..."

# Required parameters
#APP=${APP:?'APP variable missing.'}

# Default parameters
DRY_RUN=${DRY_RUN:="false"}

APP="surplus-tempaim-nodeweb"
APP_DIR="application-nodeweb"
NS="surplus-tempaim"
BRANCH="us-west-2"
REPO_URL="git@bitbucket.org:vopteam/deployments.git"
DEST_DIR="deployments"
DEPLOY_DIR="${DEST_DIR}/applications/surplus"
KUBECONFIG="${DEPLOY_DIR}/../kubeconfig"
run git clone -b ${BRANCH} ${REPO_URL} ${DEST_DIR}  
info ${BITBUCKET_BRANCH}
TAG="SL-XXX88-RC.1"
sed -i "/Tag\: \"SL.*cli/! s/Tag\: \"SL.*\"/Tag\: \"$TAG\"/g; s/Tag\: \"SL.*cli\"/Tag\: \"$TAG-cli\"/g" $DEPLOY_DIR/${NS}-config.yaml 
#run helm upgrade surplus-tempaim-nodeweb ./application-nodeweb --install --namespace surplus-tempaim --values surplus-tempaim-values.yaml --values surplus-tempaim-config.yaml $@
#run cat $DEPLOY_DIR/${NS}-config.yaml 
run helm upgrade ${APP} ${DEPLOY_DIR}/${APP_DIR} --install --namespace ${NS} --kubeconfig=$KUBECONFIG --values ${DEPLOY_DIR}/${NS}-values.yaml --values $DEPLOY_DIR/${NS}-config.yaml --dry-run



run echo "${APP}"
run helm version

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
